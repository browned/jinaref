def main():
    """Entry point for the script "jinaref"."""
    
    import argparse
    from os import getenv
    from os.path import splitext
    from jinaref import make_labels, process_citations, write_tex_file
    
    parser = argparse.ArgumentParser()
    parser.add_argument("bibfile",help="Name of bibtex file, for example references.bib.",default=None)
    parser.add_argument("-m","--max-names",help="Maximum number of author names to print in a citation; entries with more authors than this will be listed as 'first author et al.' DEFAULT: 99",default=99)
    parser.add_argument("-t","--tex-file",help="LaTeX file for References section. DEFAULT: citations.tex",default="citations.tex")
    parser.add_argument("-x","--xelatex",action="store_true",default=False, help="process the references section with xelatex and biber. XeLaTeX is run in batchmode. DEFAULT: False")
    args = parser.parse_args()

    bibtex_file = args.bibfile
    bibstem,ext = splitext(bibtex_file)
    if not ext:
        bibtex_file = bibstem+'.bib'
    new_bibtex_file = bibstem+'-labeled'+'.bib'
    max_names = args.max_names
    tex_file = args.tex_file
    texstem,ext = splitext(tex_file)
    if not ext:
        tex_file = texstem+'.tex'
    
    make_labels(bibtex_file,new_bibtex_file)

    print('writing',tex_file,'...')
    write_tex_file(tex_file,max_names,new_bibtex_file)
    
    if args.xelatex:
        print('processing',texstem)
        process_citations(texstem)

if __name__ == '__main__':
    main()