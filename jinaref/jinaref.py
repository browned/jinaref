import subprocess
import string
import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter

latex_template =\
r"""% !TEX encoding = UTF-8 Unicode
% !BIB TS-program = biber
% !TEX TS-program = xelatex
\documentclass[10pt,twoside]{{JINAproposal}}
\hypersetup{{ %
  pdfencoding = auto, %
  pdftitle={{JINA-CDN.citations}},
  pdfauthor={{Hendrik Schatz, PI}},
  colorlinks,
  citecolor={{MSBlue}},
  urlcolor={{MSBlue}},
  linkcolor={{MSBlue}}}}

% for formatting the labels using biber
\usepackage[backend=biber,style=alphabetic,labelalpha=true,maxbibnames={0},giveninits=true]{{biblatex}}
\addbibresource{{{1}}}

% Template for the labels. If the "label" field is provided, use that.
% Otherwise, construct label from the first 3 letters of first author surname 
% and 2 digits from the year.
\DeclareLabelalphaTemplate{{
  \labelelement{{
    \field[final]{{label}}
    \field[strwidth=3,strside=left,names=1,noalphaothers=true]{{labelname}}
  }}
  \labelelement{{
    \field[strwidth=2,strside=right]{{year}}
  }}
}}

\newcommand{{\JINArefname}}{{\color{{MSBlue}} References Cited}}

\begin{{document}}
\nocite{{*}}
\newpage
\printbibliography[title=\JINArefname]
\end{{document}}
"""

def write_tex_file(tex_file,maxnames,bibtex_file):
    with open(tex_file,'w') as tex:
        tex.write(latex_template.format(maxnames,bibtex_file))    

def make_labels(bibtex_file,labeled_bibtex_file):

    print('loading',bibtex_file,":")
    print("  removing abstracts and extra url's")
    print("  changing months to integers")
    bib_database = load_bib_file(bibtex_file)
    
    print('checking for duplicates')
    entries = remove_duplicates(bib_database.entries)

    print('constructing labels...')
    for record in bib_database.entries:
        record['label'] = record['ID']
    
    print('writing records to ',labeled_bibtex_file,'...')
    writer = BibTexWriter()
    writer.order_entries_by = 'label','author','year'
    writer.indent = '  '
    with open(labeled_bibtex_file,'w') as bf:
        bibtexparser.dump(bib_database,bf,writer)
    print('done')

def load_bib_file(filename):
    parser = BibTexParser(common_strings=True)
    parser.ignore_nonstandard_types = False
    parser.customization = customizations
    with open(filename) as bibtex_file:
        bib_database = bibtexparser.load(bibtex_file,parser=parser)
    return bib_database

def customizations(record):
    record = remove_abstract(record)
    record = trim_doi_url(record)
    record = adjust_month(record)
    return record

def remove_abstract(record):
    if 'abstract' in record:
        del record['abstract']
    return record
    
def trim_doi_url(record):
    if 'url' in record:
        if 'doi' in record or 'eprint' in record:
            del record['url']
    return record

def adjust_month(record):
    months = {
        'jan':1,
        'feb':2,
        'mar':3,
        'apr':4,
        'may':5,
        'jun':6,
        'jul':7,
        'aug':8,
        'sep':9,
        'oct':10,
        'nov':11,
        'dec':12
    }
    if 'month' in record:
        if record['month']:
            month = record['month'].lstrip('{').rstrip('}').lower()
            try:
                m = months[month[0:3]]
                record['month'] = str(m)
            except(KeyError):
                pass
        else:
            del record['month']
    return record

def remove_duplicates(entries):
    duplicates = {}
    labels = set()
    # first pass: mark duplicates
    for record in entries:
        label = record['ID']
        if label in labels:
            duplicates[label] = 0
        labels.add(label)
    # if duplicates were found, do a second pass and adjust IDs
    if len(duplicates) > 0:
        for record in entries:
            label = record['ID']
            if label in duplicates:
                newlabel = label+string.ascii_lowercase[duplicates[label]]
                print('moving',record['ID'],'to',newlabel)
                record['ID'] = newlabel
                duplicates[label] += 1
    return entries

def process_citations(texname):
    xelatex = [ 'xelatex', '-interaction=batchmode', texname ]
    biber = ['biber',texname]
    try:
        subprocess.run(xelatex,check=True)
        subprocess.run(biber,check=True)
        subprocess.run(xelatex,check=True)
        subprocess.run(xelatex,check=True)
    except(subprocess.CalledProcessError):
        print('unable to TeX',texname)
