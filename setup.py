from setuptools import setup

setup(
    name='jinaref',
    version='1.0.0',
    description='produces a list of references in JINA-MS style from a bibtex file',
    author='Edward Brown',
    author_email = 'browned@msu.edu',
    license = 'MIT',
    packages = ['jinaref'],
    install_requires = ['bibtexparser'],
    entry_points = {
        'console_scripts': ['jinaref=jinaref.api:main']},
    zip_safe=True
)