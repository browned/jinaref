# JINAref

This lightweight tool produces from a bibtex file a list of references (Tex/pdf) in the format used in the JINA proposals. Each citation is labeled using the bibtex key, since that is fixed in the proposal text.

## Prerequisites

1. A standard Python (v3+) installation, e.g., Anaconda.  You will need the `pip` installer.
2. A TeX installation that includes `xelatex`, `biblatex`, and `biber`.
3. The Arial, Arial Narrow, and Courier fonts.

## Installation

1. From within the top-level directory (containing `setup.py`), execute

        pip install .

    This will install the jinaref class files and the `bibtexparser` package into your distribution and make a command-line tool, `jinaref`.
    
2. Test the installation

        cd tests
        ./test_jinaref
    
    If you want to test the xelatex processing, do `./test_jinaref -x` and compare the output to `citations-org.pdf`.

## Usage

The basic usage (assuming your bibtex file is names `references.bib`)

    $ jinaref references

To see options, do

    $ jinaref --help
    
which produces

    usage: jinaref [-h] [-m MAX_NAMES] [-t TEX_FILE] [-x] bibfile

    positional arguments:
      bibfile               Name of bibtex file, for example references.bib.

    optional arguments:
      -h, --help            show this help message and exit
      -m MAX_NAMES, --max-names MAX_NAMES
                            Maximum number of author names to print in a citation;
                            entries with more authors than this will be listed as
                            'first author et al.' DEFAULT: 99
      -t TEX_FILE, --tex-file TEX_FILE
                            LaTeX file for References section. DEFAULT:
                            citations.tex
      -x, --xelatex         process the references section with xelatex and biber.
                            XeLaTeX is run in batchmode. DEFAULT: False
