%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% proposal.cls - style for formatting proposals
%
\def\fileversion{1.0}
\def\filedate{2020/01/22}
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{JINAproposal}[\filedate, \fileversion]
\RequirePackage{ifxetex}
\RequirePackage{xifthen}
\typeout{%
  Document Style: `JINAproposal' v\fileversion \space <\filedate>}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions
\LoadClass{article}

\RequirePackage{hyperref}

\RequirePackage[x11names]{xcolor}
\definecolor{MSBlue}{RGB}{46,83,150}
\RequirePackage{graphicx}

\ifxetex
    \RequirePackage{mathspec}
\else
    \RequirePackage{helvet}
\fi

\RequirePackage{aasjournals}
\RequirePackage{fancyhdr}

\ifxetex
	\typeout{loading Arial, Courier, and Arial Narrow}
	\setmainfont[Mapping=tex-text]{Arial}
	\setmonofont[Mapping=tex-text,Scale=0.95]{Courier}
	\setsansfont[Mapping=tex-text,Scale=MatchLowercase]{Arial Narrow}
\else
    \typeout{setting main font to Helvetica}
    \renewcommand{\familydefault}{\sfdefault}
\fi

\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}
\setlength{\textwidth}{468bp}
\setlength{\textheight}{608bp}
\setlength{\headsep}{20pt}      %decrease head separation slightly
\setlength{\topmargin}{0in}
\renewcommand{\headrulewidth}{0pt}
\pagestyle{fancy}               % page layout
\fancyhf{}
\fancyfoot[C]{References Cited - \thepage}

\providecommand{\mathplus}{\ensuremath{+}}
